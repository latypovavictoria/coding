#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <cstring>
#include <stdlib.h>
bool Exist(char* filename) {
	FILE* file = fopen(filename, "rb");
	if (file == NULL) {
		printf("File does not exist!\n");
		fclose(file);
		return false;
	}
	else {
		fclose(file);
		return true;
	}
}
unsigned int Generate_gamma(unsigned int seed) {
	srand(seed);
	unsigned int gamma = rand();
	return gamma;
}
unsigned int Count_size(char* filename) {
	FILE* file = fopen(filename, "rb");
	unsigned int count = 0;
	char symbol;
	for (symbol = getc(file); !feof(file); symbol = getc(file)) {
		count = count + 1;
	}
	fclose(file);
	return count;
}
void Encrypt(char* filename_in, char* filename_out, unsigned int gamma) {
	FILE* file_in = fopen(filename_in, "rb");
	FILE* file_out = fopen(filename_out, "wb");
	unsigned int size_in = Count_size(filename_in);
	char* Res = (char*)malloc((size_in + 1) * sizeof(char));
	
	char* info = (char*)malloc((size_in + 1) * sizeof(char));
	
	unsigned int i = 0;
	char* byte = (char*)malloc(4 * sizeof(char));
	char* byte_key = (char*)malloc(size_in * sizeof(char));
	*((unsigned int*)byte) = gamma;
	for (i = 0; i < size_in; i++) {
		strcpy(byte_key, byte);
	}
	for (i = 0; i < size_in; i++) {
		fread(&info[i], sizeof(char), 1, file_in);
		Res[i] = info[i] ^ byte_key[i];
		fwrite(&Res[i], sizeof(char), 1, file_out);
	}
	free(Res);
	free(info);
	fclose(file_in);
	fclose(file_out);
}
char* Get() {
	char* filename_in = (char*)malloc(sizeof(char));
	scanf("%s", filename_in);
	//free(filename_in);
	return filename_in;
}
int main() {
	bool exist;
	char* filename_in;
	unsigned int seed;
	const char meta[] = "_encrypted";
	char* filename_out;
	size_t size_in;
	size_t size_meta;
	printf("\nInput filename:");
	filename_in = Get();
	exist = Exist(filename_in);
	if (exist == true) {
		size_in = strlen(filename_in);
		size_meta = strlen(meta);
		filename_out = (char*)malloc((size_in + size_meta + 1) * sizeof(char));
		
		memcpy(filename_out, filename_in, size_in);
		memcpy(filename_out + size_in, meta, size_meta + 1);//strcat!
		printf("%s\n", filename_out);
		printf("\nInput the key:");
		scanf("%u", &seed);
		Encrypt(filename_in, filename_out, Generate_gamma(seed));
		free(filename_out);
	}
	else {
		printf("Such file does not exist!");
	}
	return 0;
}

/*int main(){
char test[]="wgqduyheuwiyuiewyruwr djfrhgjdfhgjk 85476945876";
unsigned int seed;
char res[48];
char res2[48];
int i;
scanf("%u", &seed);
for(i=0;i<48;i++){
res[i]=test[i]^Generate_gamma(seed);
printf("%c", res[i]);
}
printf("\n");
for(i=0;i<48;i++){
res2[i]=res[i]^Generate_gamma(seed);
printf("%c", res2[i]);
}
}*/
